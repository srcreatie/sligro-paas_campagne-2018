function setData(callback) {

    var devDynamicContent = {};

    version = "v1" ; 

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].style = {};

    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "De heerlijkste voorjaarsproducten";
    devDynamicContent.srFeed[0].copy.h2 = "De mooiste recepten";
    devDynamicContent.srFeed[0].copy.h1_728 = "De<br>heerlijkste<br>voorjaarsproducten";
    devDynamicContent.srFeed[0].copy.h2_728 = "De<br>mooiste<br>recepten";
    devDynamicContent.srFeed[0].copy.cta = "Lees meer";
    
    devDynamicContent.srFeed[0].fr1_300x250 = dimension("300x250-broodtaart.jpg", "300x250");
    devDynamicContent.srFeed[0].fr2_300x250 = dimension("300x250-kwartelei.jpg", "300x250");
    devDynamicContent.srFeed[0].fr3_300x250 = dimension("300x250-kreeft.jpg", "300x250");
    devDynamicContent.srFeed[0].fr4_300x250 = dimension("300x250-ijs.jpg", "300x250");

    devDynamicContent.srFeed[0].fr1_336x280 = dimension("336x280-broodtaart.jpg", "336x280");
    devDynamicContent.srFeed[0].fr2_336x280 = dimension("336x280-kwartelei.jpg", "336x280");
    devDynamicContent.srFeed[0].fr3_336x280 = dimension("336x280-kreeft.jpg", "336x280");
    devDynamicContent.srFeed[0].fr4_336x280 = dimension("336x280-ijs.jpg", "336x280");

    devDynamicContent.srFeed[0].fr1_728x90 = dimension("728x90-broodtaart.jpg", "728x90");
    devDynamicContent.srFeed[0].fr2_728x90 = dimension("728x90-kwartelei.jpg", "728x90");
    devDynamicContent.srFeed[0].fr3_728x90 = dimension("728x90-kreeft.jpg", "728x90");
    devDynamicContent.srFeed[0].fr4_728x90 = dimension("728x90-ijs.jpg", "728x90");

    devDynamicContent.srFeed[0].fr1_970x250 = dimension("970x250-broodtaart.jpg", "970x250");
    devDynamicContent.srFeed[0].fr2_970x250 = dimension("970x250-kwartelei.jpg", "970x250");
    devDynamicContent.srFeed[0].fr3_970x250 = dimension("970x250-kreeft.jpg", "970x250");
    devDynamicContent.srFeed[0].fr4_970x250 = dimension("970x250-ijs.jpg", "970x250");

    devDynamicContent.srFeed[0].fr1_300x600 = dimension("300x600-broodtaart.jpg", "300x600");
    devDynamicContent.srFeed[0].fr2_300x600 = dimension("300x600-kwartelei.jpg", "300x600");
    devDynamicContent.srFeed[0].fr3_300x600 = dimension("300x600-kreeft.jpg", "300x600");
    devDynamicContent.srFeed[0].fr4_300x600 = dimension("300x600-ijs.jpg", "300x600");
    
    return devDynamicContent;
}

module.exports = setData;
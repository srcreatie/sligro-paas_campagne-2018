function setup(callback) {

    Enabler.setProfileId(1079748);
    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "USING GOOGLE DYNAMIC FEED 3";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].background = asset("dynamic.png");
    devDynamicContent.srFeed[0].logo = asset("logo.png");
    devDynamicContent.srFeed[0].endscreen = asset("endscreen-offer.png");

    if (srBanner.debug) {

        console.log("%c ==============feed=============", 'background: #0060a1; color: #FFFFFF');
        console.log("USING GOOGLE DYNAMIC FEED");
        console.log("%c _______________________________", 'background: #0060a1; color: #FFFFFF');

    }

    Enabler.setDevDynamicContent(devDynamicContent);

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {

        var dd = dynamicContent.srFeed[0];

        dcsDataSetupDone(dd)

    }

    function dcsDataSetupDone(dd) {

        callback(dd);

    }

}

module.exports.setup = setup;
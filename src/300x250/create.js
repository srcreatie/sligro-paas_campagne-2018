function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('RockwellStd_regular.woff'),
            asset('UniversLTStd_Bold.woff')
        ], add);
    }

    function add() {
        ___("bg_fr1")
            .image(dd.fr1_300x250, {width:config.bannerWidth, height:config.bannerHeight, fit:true})
            .position({top:0, left:0});

        ___("bg_fr2")
            .image(dd.fr2_300x250, {width:config.bannerWidth, height:config.bannerHeight, fit:true})
            .position({top:0, left:0});

        ___("bg_fr3")
            .image(dd.fr3_300x250, {width:config.bannerWidth, height:config.bannerHeight, fit:true})
            .position({top:0, left:0});

        ___("bg_fr4")
            .image(dd.fr4_300x250, {width:config.bannerWidth, height:config.bannerHeight, fit:true})
            .position({top:0, left:0});

        ___("white_footer")
            .style({width:config.bannerWidth, height:60, background:"#FFFFFF", greensock:{ alpha:0.75 }})
            .position({left:0, bottom:0});

        ___("green_block")
            .style({width:90, height:46, background:"#009775"})
            .position({el:__("white_footer"), left:6, centerY:0});

        ___("paas_kv_small")
            .image(asset("lente-kv.png"), {width:68, height:29, fit:true})
            .position({el:__("green_block"), centerX:0, centerY:0});

        ___("h1")
            .text(dd.copy.h1, {webfont:"RockwellStd_regular", fontSize:14, color:"#000000", textAlign:"left", upperCase:true})
            .position({el:__("green_block"), centerY:-5, push:{el:__("green_block"), right:15}});

        ___("h2")
            .text(dd.copy.h2, {webfont:"RockwellStd_regular", fontSize:14, color:"#000000", textAlign:"left", width:100, upperCase:true})
            .position({el:__("green_block"), centerY:0, push:{el:__("green_block"), right:15}});

        ___("logo")
            .image(asset("sligro-logo.png"), {width:77, height:44, fit:true})
            .position({top:10, left:10});

        ___("green_footer")
            .style({width:config.bannerWidth, height:68, background:"#009775", greensock:{ alpha:0.75 }})
            .position({centerX:0, bottom:5});

        ___("paas_kv")
            .image(asset("lente-kv.png"), {width:114, height:48, fit:true})
            .position({el:__("green_footer"), centerX:0, centerY:0});

        ___("cta")
            .text(dd.copy.cta, {webfont:"UniversLTStd_Bold", fontSize:12, color:"#FFFFFF", upperCase:true, addClass:"cta"})
            .position({bottom:15, right:15});

        ___("cta_arrow")
            .image(asset("cta-arrow.png"), {width:10, height:10, fit:true})
            .position({el:__("cta"), centerY:0, push:{el:__("cta"), left:-22}});

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;
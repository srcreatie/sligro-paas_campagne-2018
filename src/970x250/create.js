function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('RockwellStd_regular.woff'),
            asset('UniversLTStd_Bold.woff')
        ], add);
    }

    function add() {
        ___("bg_fr1")
            .image(dd.fr1_970x250, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("bg_fr2")
            .image(dd.fr2_970x250, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("bg_fr3")
            .image(dd.fr3_970x250, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("bg_fr4")
            .image(dd.fr4_970x250, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("white_footer")
            .style({width:300, height:170, background:"#FFFFFF", greensock:{ alpha:0.75 }})
            .position({right:0, top:0});

        ___("green_block")
            .style({width:300, height:80, background:"#009775"})
            .position({right:0, bottom:0});

        ___("paas_kv_small")
            .image(asset("lente-kv.png"), {width:100, height:42, fit:true})
            .position({el:__("green_block"), centerX:0, centerY:0});

        ___("h1")
            .text(dd.copy.h1, {webfont:"RockwellStd_regular", fontSize:22,width:290, color:"#000000", textAlign:"center", upperCase:true})
            .position({el:__("white_footer"), centerX:0, centerY:0});

        ___("h2")
            .text(dd.copy.h2, {webfont:"RockwellStd_regular", fontSize:22, color:"#000000", textAlign:"center", width:290, upperCase:true})
            .position({el:__("white_footer"), centerX:0, centerY:0});

        ___("logo")
            .image(asset("sligro-logo.png"), {width:86, height:49, fit:true})
            .position({top:10, left:10});

        ___("green_footer")
            .style({width:400, height:250, background:"#009775", greensock:{ alpha:0.75 }})
            .position({right:0, bottom:0});

        ___("paas_kv")
            .image(asset("lente-kv.png"), {width:220, height:92, fit:true})
            .position({el:__("green_footer"), centerX:0, centerY:0});

        ___("cta")
            .text(dd.copy.cta, {webfont:"UniversLTStd_Bold", fontSize:16, color:"#FFFFFF", upperCase:true, addClass:"cta"})
            .position({el:__("white_footer"), centerX:0, centerY:0});

        ___("cta_arrow")
            .image(asset("cta-arrow.png"), {width:10, height:10, fit:true})
            .position({el:__("cta"), centerY:0, push:{el:__("cta"), left:-22}});

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;
require('./../lib/sr.dynamic.min.js');
var setConfig = require('./../set/config.js');
var create = require('./create.js');
var interact = require('./interact.js');

var srBanner = setConfig.loadSettings();
var dynamicData = srBanner.dynamicData;



if (srBanner.debug) {
    console.log('%c ==============SR Banner props=============', 'background: #0060a1; color: #FFFFFF');
    // console.log('%c Oh my heavens! ', 'background: #222; color: #bada55');
    if(console.table){

        console.table(srBanner, ["Value"]);
    } else {

        console.log(srBanner); 
    }

    console.log('%c __________________________________________', 'background: #0060a1; color: #FFFFFF');
}

window.onload = setupBanner;

function setupBanner() {

    //load data 
    dynamicData.setup(function(dynamicData) {

        //data loaded, create global var dd and fill it with data 
        dd = dynamicData;

        //create and set elements
        create.setElements(function() {

            //after set start setup of animation
            interact.animate();

        });
    });
}
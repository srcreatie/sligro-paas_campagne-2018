function animate(callback) {

    setAnimation();

    function setAnimation() {
        
        var tl01 = new TimelineMax();        
        var tlRO = new TimelineMax({ paused : true });

        tl01.set(__("creative"), { alpha:1 });

        // First frame
        tl01.to(bg_fr1, 2.6, {scale:1.2, transformOrigin:"center center", ease:Power0.easeNoneut}, "fr1");
        tl01.to([green_footer, paas_kv], 0.8, {alpha:0}, "fr1+=2");
        tl01.to(bg_fr1, 0.6, {alpha:0}, "fr1+=2.4");

        // Frame 2
        tl01.from(bg_fr2, 1.4, {alpha:0}, "fr2-=1.1");
        tl01.to(bg_fr2, 3.2, {scale:1.2, transformOrigin:"center center", ease:Power0.easeNone}, "fr2-=1.1");
        tl01.from(white_footer, 0.8, {width:0, ease:Expo.easeOut}, "fr2-=0.4");
        tl01.from([green_block, paas_kv_small, h1], 0.8, {alpha:0}, "fr2-=0.4");

        tl01.to(bg_fr2, 0.6, {alpha:0}, "fr2+=1.9");

        // Frame 3
        tl01.from(bg_fr3, 1.4, {alpha:0}, "fr3-=1.1");
        tl01.to(bg_fr3, 3.2, {scale:1.2, transformOrigin:"center center", ease:Power0.easeNone}, "fr3-=1.1");
        tl01.to(h1, 0.4, {alpha:0, y:20}, "fr3-=1");
        tl01.from(h2, 0.8, {alpha:0, y:-10}, "fr3-=0.8");

        tl01.to(bg_fr3, 0.6, {alpha:0}, "fr3+=1.9");

        // Frame 4
        tl01.from(bg_fr4, 1.4, {alpha:0}, "fr4-=1.1");
        tl01.to(bg_fr4, 3.2, {scale:1.2, transformOrigin:"center center", ease:Sine.easeOut}, "fr4-=1.1");
        tl01.to(h2, 0.6, {alpha:0, x:60}, "fr4-=0.8");
        tl01.from(cta, 0.8, {x:-100, alpha:0}, "fr4-=0.6");
        tl01.from(cta_arrow, 0.4, {x:-20, alpha:0, onComplete:rollover}, "fr4+=0.2");

        // Rollover
        tlRO.to(bg_fr4, 5, {scale:1, ease:Sine.easeInOut, transformOrigin:"center center"}, "rollover");
        tlRO.to(cta_arrow, 0.4, {x:92, ease:Power3.easeInOut}, "rollover");
        tlRO.to(cta, 0.4, {css:"padding:12px 30px 8px 15px;", ease:Power3.easeInOut}, "rollover");

        function rollover(){
            __("banner").onmouseover = function(){
                tlRO.play(0);
            };

            __("banner").onmouseout = function(){
                tlRO.reverse();
            };
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;
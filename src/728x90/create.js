function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('RockwellStd_regular.woff'),
            asset('UniversLTStd_Bold.woff')
        ], add);
    }

    function add() {
        ___("bg_fr1")
            .image(dd.fr1_728x90, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("bg_fr2")
            .image(dd.fr2_728x90, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("bg_fr3")
            .image(dd.fr3_728x90, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("bg_fr4")
            .image(dd.fr4_728x90, {width:config.bannerWidth, height:config.bannerHeight, wrap:true})
            .position({top:0, left:0});

        ___("white_footer")
            .style({width:320, height:90, background:"#FFFFFF", greensock:{ alpha:0.75 }})
            .position({right:0, top:0});

        ___("green_block")
            .style({width:100, height:52, background:"#009775"})
            .position({left:418, centerY:0});

        ___("paas_kv_small")
            .image(asset("lente-kv.png"), {width:76, height:32, fit:true})
            .position({el:__("green_block"), centerX:0, centerY:0});

        ___("h1")
            .text(dd.copy.h1_728, {webfont:"RockwellStd_regular", fontSize:15,width:120, color:"#000000", textAlign:"left", upperCase:true, css:"padding-top:5px;"})
            .position({ left:530, centerY:0});

        ___("h2")
            .text(dd.copy.h2_728, {webfont:"RockwellStd_regular", fontSize:15, color:"#000000", textAlign:"left", width:290, upperCase:true, css:"padding-top:5px;"})
            .position({ left:530, centerY:0});

        ___("logo")
            .image(asset("sligro-logo.png"), {width:101, height:58, fit:true})
            .position({centerY:0, left:10});

        ___("green_footer")
            .style({width:250, height:90, background:"#009775", greensock:{ alpha:0.75 }})
            .position({right:0, bottom:0});

        ___("paas_kv")
            .image(asset("lente-kv.png"), {width:146, height:60, fit:true})
            .position({el:__("green_footer"), centerX:0, centerY:0});

        ___("cta")
            .text(dd.copy.cta, {webfont:"UniversLTStd_Bold", fontSize:16, color:"#FFFFFF", upperCase:true, addClass:"cta"})
            .position({el:__("white_footer"), right:20, centerY:0});

        ___("cta_arrow")
            .image(asset("cta-arrow.png"), {width:10, height:10, fit:true})
            .position({el:__("cta"), centerY:0, push:{el:__("cta"), left:-22}});

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;